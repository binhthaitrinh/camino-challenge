# Camino Financial's Backend Take-home Challenge

A RESTful API that takes an online application for a business that wants a loan

## Getting Started

In for project directory, you can run

### `source env/bin/activate`

### `pip install requirements.txt`

### `python manage.py runserver` to run server on local machine

### `python mange.py test` to run tests

## Technologies Used

- Python Django Framework
- Python Djago REST Framework
- Gitlab CI/CD
- Heroku for deployment

## Implmentation

1. Endpoint `loanapp/` to take application. Can consume the required json
2. POST action to `loanapp/` can recognize duplications in app submissions based on Business Name and TaxID. Since these two are not easily changed in real life, the are used as the criteria for detecting duplicate submissions. Action to be taken when receiving duplication submissions: update existing database objects without creating a new one
3. Endpoint `status/` to provide a status on an application submitted given a loanapp id.

   - The LoanID is supposed to be tied together with actual Loan id existing in databsae. However, due to time constraints, I have not been able to get this to work.

4. Database used: Postgresql
5. Set up Gitlab CI/CD to automatically run test and deploy to Heroku
6. API deployed to [Here](http://evening-headland-54638.herokuapp.com/loanapp/).
7. Current issues
   - Test passed on local machine, but could not pass on Gitlab CI/CD

## Experience, thoughts on the project

To be honest, this is my first time developing in Python, not to mention Django. I was more familiar with ExpressJS, NodeJS and MongoDB. I have spent quite a lot of time dedicating to learn about the framework and work on the project. I am not quite satisfied with the result. But this is all I have for now. This challenge has been a great opportunity for me to learn about a new framework, and it has been a fun challenge. No matter the result, I am happy with what I have learned so far, and will improve on it in the future. Thank you for the opportunity!

### Contributors

- Binh Trinh - Initial Work
