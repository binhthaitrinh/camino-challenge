from django.contrib import admin
from loanapp import models

# Register your models here.
admin.site.register(models.Loan)
admin.site.register(models.LoanStatus)
