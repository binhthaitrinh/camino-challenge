from django.urls import path, include
from rest_framework.routers import DefaultRouter

from loanapp import views

router = DefaultRouter()

router.register('status', views.LoanStatusViewSet)
router.register('loanapp', views.LoanViewSet)


app_name = 'loanapp'

urlpatterns = [
    path('', include(router.urls))
]
