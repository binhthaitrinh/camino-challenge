from rest_framework import serializers

from loanapp.models import RequestHeader, Loan, Business, Owner, \
    Address, CFApplicationData, SelfReportedCashFlow, LoanStatus

from rest_framework.exceptions import NotFound


class SelfReportedCashFlowSerializers(serializers.ModelSerializer):
    """ Serializer for Self Reported Cash Flow """

    class Meta:
        model = SelfReportedCashFlow
        fields = ('AnnualRevenue', 'MonthlyAverageBankBalance',
                  'MonthlyAverageCreditCardVolume')


class RequestHeaderSerializers(serializers.ModelSerializer):
    """ Serializer for Request Header """

    class Meta:
        model = RequestHeader
        fields = ('CFRequestId', 'RequestDate', 'CFApiUserId',
                  'CFApiPassword', 'IsTestLead')


class AddressSerializers(serializers.ModelSerializer):
    """ Serializer for Address """

    class Meta:
        model = Address
        fields = ('Address1', 'Address2', 'City', 'State', 'Zip')


class BusinessSerializers(serializers.ModelSerializer):
    """ Serializer for Business """

    Address = AddressSerializers(many=False)
    SelfReportedCashFlow = SelfReportedCashFlowSerializers(many=False)

    class Meta:
        model = Business
        fields = ('Name', 'TaxID', 'Phone', 'NAICS', 'HasBeenProfitable',
                  'HasBankruptedInLast7Years', 'InceptionDate', 'Address', 'SelfReportedCashFlow')


class OwnerSerializers(serializers.ModelSerializer):
    """ Serializer for Owner """

    HomeAddress = AddressSerializers(many=False)

    class Meta:
        model = Owner
        fields = ('Name', 'FirstName', 'LastName', 'Email',
                  'DateOfBirth', 'HomePhone', 'SSN', 'PercentageOfOwnership', 'HomeAddress')


class CFAppDataSerializers(serializers.ModelSerializer):
    """ Serializer for CFAppData """

    class Meta:
        model = CFApplicationData
        fields = ('RequestedLoanAmount', 'StatedCreditHistory',
                  'LegalEntityType', 'FilterID')


class LoanSerializers(serializers.ModelSerializer):
    """ Serializer for Loan """

    RequestHeader = RequestHeaderSerializers(many=False)
    Business = BusinessSerializers(many=False)
    Owners = OwnerSerializers(many=True)
    CFApplicationData = CFAppDataSerializers(many=False)

    class Meta:
        model = Loan
        fields = ('id', 'RequestHeader', 'Business',
                  'Owners', 'CFApplicationData')
        read_only_fields = ('id',)

    def create(self, validated_data):
        """ Create new Loan object """

        # Popping data for object creation
        request_data = validated_data.pop('RequestHeader')
        business_data = validated_data.pop('Business')
        cfApp_data = validated_data.pop('CFApplicationData')
        business_address_data = business_data.pop('Address')
        self_cash_flow_data = business_data.pop('SelfReportedCashFlow')
        owners_data = validated_data.pop('Owners')

        # Create objects
        business_address = Address.objects.create(**business_address_data)
        self_cash_flow = SelfReportedCashFlow.objects.create(
            **self_cash_flow_data)
        request = RequestHeader.objects.create(
            **request_data)
        business = Business.objects.create(
            **business_data, Address=business_address, SelfReportedCashFlow=self_cash_flow)
        cfApp = CFApplicationData.objects.create(**cfApp_data)
        loan = Loan.objects.create(
            **validated_data, RequestHeader=request, Business=business, CFApplicationData=cfApp)

        # Create and fill owners data
        for owner_data in owners_data:
            home_address_data = owner_data.pop('HomeAddress')
            home_address = Address.objects.create(**home_address_data)
            Owner.objects.create(Loan=loan, **owner_data,
                                 HomeAddress=home_address)

        return loan

    def update(self, instance, validated_data):
        """ explicit update existing object """

        # Popping data for object creations
        request_data = validated_data.pop('RequestHeader')
        business_data = validated_data.pop('Business')
        business_address_data = business_data.pop('Address')
        self_cash_flow_data = business_data.pop('SelfReportedCashFlow')
        owners_data = validated_data.pop('Owners')
        cfApp_data = validated_data.pop('CFApplicationData')

        # Create objects
        RequestHeader.objects.update(**request_data)
        Address.objects.update(**business_address_data)
        SelfReportedCashFlow.objects.update(**self_cash_flow_data)
        Business.objects.update(**business_data)
        cfApp = CFApplicationData.objects.update(**cfApp_data)

        # Prepare and create data for owners
        owners = (instance.Owners).all()
        owners = list(owners)

        for owner_data in owners_data:
            home_address_data = owner_data.pop('HomeAddress')
            Address.objects.update(**home_address_data)
            owner = owners.pop(0)
            owner.Name = owner_data.get('Name')
            FirstName = owner_data.get('FirstName')
            LastName = owner_data.get('LastName')
            Email = owner_data.get('Email')
            DateOfBirth = owner_data.get('DateOfBirth')
            HomePhone = owner_data.get('HomePhone')
            SSN = owner_data.get('SSN')
            PercentageOfOwnership = owner_data.get('PercentageOfOwnership')

            owner.save()

        instance.save()
        return instance


class LoanStatusSerializers(serializers.ModelSerializer):
    """ Serializer for LoanStatus """

    class Meta:
        model = LoanStatus
        fields = ('LoanID', 'StatusMessage', 'Status', 'id')
        read_only_fields = ('id',)
