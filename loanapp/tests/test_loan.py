from django.urls import reverse
from django.test import TestCase
import json

from rest_framework import status
from rest_framework.test import APIClient

from loanapp.models import Loan, RequestHeader, Business, Owner, CFApplicationData, Address, SelfReportedCashFlow
from loanapp.serializers import LoanSerializers


LOAN_URL = '/loanapp/'


def create_data_helper(sample_data):
    """ Helper function to create a new Loan object """

    # Create RequestHeader object from sample_data
    request = RequestHeader.objects.create(**sample_data.pop('RequestHeader'))

    # Create SelfReportedCashFlow object from sample_data
    cash_flow = SelfReportedCashFlow.objects.create(
        **sample_data['Business'].pop('SelfReportedCashFlow'))

    # Create Address for Business object from sample_data
    business_address = Address.objects.create(
        **sample_data['Business'].pop('Address'))

    # Create Business from sample_data
    business = Business.objects.create(
        **sample_data.pop('Business'), Address=business_address, SelfReportedCashFlow=cash_flow)

    # Create CFApplicationData object from sample_data
    cf_application_data = CFApplicationData.objects.create(
        **sample_data.pop('CFApplicationData'))

    # Take owner data from sample_data
    owners_data = sample_data.pop('Owners')

    # Create Loan object from sample_data
    loan = Loan.objects.create(
        **sample_data, RequestHeader=request, Business=business, CFApplicationData=cf_application_data)

    # Create each individual Owner Object and save into Loan object
    for owner_data in owners_data:
        home_address_data = owner_data.pop('HomeAddress')
        home_address = Address.objects.create(**home_address_data)
        Owner.objects.create(Loan=loan, **owner_data,
                             HomeAddress=home_address)


class LoanApiTests(TestCase):
    """ Test for Loan API """

    def setUp(self):
        """ Set up for each test """

        self.client = APIClient()
        self.sample_data = {
            "RequestHeader": {
                "CFRequestId": "500653901",
                "RequestDate": "2019-06-26T23:05:41.2898238Z",
                "CFApiUserId": None,
                "CFApiPassword": None,
                "IsTestLead": True
            },
            "Business": {
                "Name": "Wow Inc",
                "SelfReportedCashFlow": {
                    "AnnualRevenue": 49999999.0,
                    "MonthlyAverageBankBalance": 94941.0,
                    "MonthlyAverageCreditCardVolume": 18191.0
                },
                "Address": {
                    "Address1": "1234 Red Ln",
                    "Address2": "5678 Blue Rd",
                    "City": "Santa Monica",
                    "State": "CA",
                    "Zip": "45321"
                },
                "TaxID": "839674398",
                "Phone": "6573248876",
                "NAICS": "79232",
                "HasBeenProfitable": True,
                "HasBankruptedInLast7Years": False,
                "InceptionDate": "2008-06-28T23:04:03.5507585+00:00"
            },
            "Owners": [
                {
                    "Name": "WH KennyTest",
                    "FirstName": "WH",
                    "LastName": "KennyTest",
                    "Email": "whkennytest@caminofinancial.com",
                    "HomeAddress": {
                        "Address1": "5567 North Ridge Ct",
                        "Address2": None,
                        "City": "Berkeley",
                        "State": "CA",
                        "Zip": "94704"
                    },
                    "DateOfBirth": "1955-12-18T00:00:00",
                    "HomePhone": "3451289776",
                    "SSN": "435790261",
                    "PercentageOfOwnership": 50.0
                },
                {
                    "Name": "Test DoeTest",
                    "FirstName": "Test",
                    "LastName": "DoeTest",
                    "Email": "Doetest@caminofinancial.com",
                    "HomeAddress": {
                        "Address1": "4512 East Ridge Ct",
                        "Address2": None,
                        "City": "Berkeley",
                        "State": "CA",
                        "Zip": "94704"
                    },
                    "DateOfBirth": "1955-12-18T00:00:00",
                    "HomePhone": "3107654321",
                    "SSN": "435790261",
                    "PercentageOfOwnership": 50.0
                },    {
                    "Name": "Test DoeTest",
                    "FirstName": "Test",
                    "LastName": "DoeTest",
                    "Email": "Doetest@caminofinancial.com",
                    "HomeAddress": {
                        "Address1": "4512 East Ridge Ct",
                        "Address2": None,
                        "City": "Berkeley",
                        "State": "CA",
                        "Zip": "94704"
                    },
                    "DateOfBirth": "1955-12-18T00:00:00",
                    "HomePhone": "3107654321",
                    "SSN": "435790261",
                    "PercentageOfOwnership": 50.0
                },    {
                    "Name": "Test DoeTest",
                    "FirstName": "Test",
                    "LastName": "DoeTest",
                    "Email": "Doetest@caminofinancial.com",
                    "HomeAddress": {
                        "Address1": "4512 East Ridge Ct",
                        "Address2": None,
                        "City": "Berkeley",
                        "State": "CA",
                        "Zip": "94704"
                    },
                    "DateOfBirth": "1955-12-18T00:00:00",
                    "HomePhone": "3107654321",
                    "SSN": "435790261",
                    "PercentageOfOwnership": 50.0
                },    {
                    "Name": "Test DoeTest",
                    "FirstName": "Test",
                    "LastName": "DoeTest",
                    "Email": "Doetest@caminofinancial.com",
                    "HomeAddress": {
                        "Address1": "4512 East Ridge Ct",
                        "Address2": None,
                        "City": "Berkeley",
                        "State": "CA",
                        "Zip": "94704"
                    },
                    "DateOfBirth": "1955-12-18T00:00:00",
                    "HomePhone": "3107654321",
                    "SSN": "435790261",
                    "PercentageOfOwnership": 50.0
                }
            ],
            "CFApplicationData": {
                "RequestedLoanAmount": "49999999",
                "StatedCreditHistory": 1,
                "LegalEntityType": "LLC",
                "FilterID": "897079"
            }
        }

    def test_retrieve_loan_list(self):
        """ Test retrieving a list of loans"""

        # Create a Loan object from sample_data
        create_data_helper(self.sample_data)

        # Send a GET request to loanapp/ endpoint
        res = self.client.get(LOAN_URL)

        # Retrieve all Loan objects in database
        loans = Loan.objects.all()

        # Serialize Loan object
        serializer = LoanSerializers(loans, many=True)

        # Test response status
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        # Test response data
        self.assertEqual(res.data, serializer.data)

    def test_create_loan_successful(self):
        """ Test create a new loan app"""

        # Send a POST request to loanapp/ endpoint
        new_loan_app = self.client.post(
            LOAN_URL, self.sample_data, format='json')

        # Look for Loan objects just created in database
        exists = Loan.objects.filter(pk=new_loan_app.json()['id'])

        # Test if it exists
        self.assertTrue(exists)

    def test_create_loan_invalid(self):
        """ Test creating invalid loan application """

        # Poorly constructed data
        payload = {
            'name': ''
        }

        # Try creating Loan using bad data
        res = self.client.post(LOAN_URL, payload)

        # Test for return status request
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_existing_instead_of_new(self):
        """ Test update existing object instead of creating new one """

        # Create a new Loan object
        new_loan_app = self.client.post(
            LOAN_URL, self.sample_data, format='json')

        # Try modifying unimportant data
        self.sample_data['Owners'][0]['HomePhone'] = '123456789'

        # Send a POST request with new data
        updated_loan_app = self.client.post(
            LOAN_URL, self.sample_data, format='json')

        # Retrieve new object from database
        new_id = Loan.objects.get(
            Business__TaxID=updated_loan_app.json()['Business']['TaxID'])

        # Same id, which means updating existing record
        self.assertEqual(new_loan_app.json()['id'],
                         new_id.id)

        # The new object is updated appropriately
        self.assertEqual(updated_loan_app.json(), self.sample_data)
