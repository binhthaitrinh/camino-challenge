from django.urls import reverse
from django.test import TestCase
import json

from rest_framework import status
from rest_framework.test import APIClient

from loanapp.models import LoanStatus
from loanapp.serializers import LoanStatusSerializers

LOAN_STATUS_URL = '/status/'


class LoanStatusApiTest(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_retrieve_list(self):
        """ Test for retrieving list of loan status"""

        LoanStatus.objects.create(
            LoanID=2, StatusMessage="Your application has been approved", Status=True)
        LoanStatus.objects.create(
            LoanID=3, StatusMessage="Sorry. You application has not been approved", Status=False)

        res = self.client.get(LOAN_STATUS_URL)

        loan_status = LoanStatus.objects.all()
        serializer = LoanStatusSerializers(loan_status, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_create_loan_status_successful(self):
        """ Test for creating loan status """
        payload = {
            'LoanID': 2, 'StatusMessage': 'Your Application has been approved', 'Status': True}

        self.client.post(LOAN_STATUS_URL, payload)

        exists = LoanStatus.objects.filter(LoanID=payload['LoanID']).exists()

        self.assertTrue(exists)

    def test_create_loan_status_invalid(self):
        """ Test for invalid creating loan status """

        payload = {'LoanID': ''}

        res = self.client.post(LOAN_STATUS_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
