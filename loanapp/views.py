from rest_framework import viewsets, permissions
from loanapp.models import Loan, Business, LoanStatus

from loanapp.serializers import LoanSerializers, LoanStatusSerializers
from django.http import Http404
from django.shortcuts import render


class LoanStatusViewSet(viewsets.ModelViewSet):
    """ Views for LoanStatus """

    permission_classes = [
        permissions.AllowAny
    ]

    queryset = LoanStatus.objects.all()
    serializer_class = LoanStatusSerializers


class LoanViewSet(viewsets.ModelViewSet):
    """ Views for Loan """

    permission_classes = [
        permissions.AllowAny
    ]

    queryset = Loan.objects.all()
    serializer_class = LoanSerializers

    def perform_create(self, serializer):
        """create a new loan"""

        exist = None

        # Check for existing application, create new if not duplicate
        try:
            business_name = self.request.data['Business']['Name']
            business_tax_id = self.request.data['Business']['TaxID']

            exist = Loan.objects.get(
                Business__Name=business_name, Business__TaxID=business_tax_id)

        except Loan.DoesNotExist:
            exist = None

        if (exist != None):
            serializer.update(exist, validated_data=self.request.data)
        else:
            serializer.save()
