from django.db import models

# Create your models here.
from django.db import models


class RequestHeader(models.Model):
    """Request Header model inside Loan """

    CFRequestId = models.CharField(max_length=255)
    RequestDate = models.CharField(max_length=255)
    CFApiUserId = models.IntegerField(null=True)
    CFApiPassword = models.CharField(max_length=255, null=True)
    IsTestLead = models.BooleanField()


class Address(models.Model):
    """ Address model """

    Address1 = models.CharField(max_length=255)
    Address2 = models.CharField(max_length=255, blank=True, null=True)
    City = models.CharField(max_length=255, blank=True)
    State = models.CharField(max_length=10, blank=True)
    Zip = models.CharField(max_length=10)


class Business(models.Model):
    """ Business model inside Loan """

    Name = models.CharField(max_length=255)
    TaxID = models.CharField(max_length=255)
    Phone = models.CharField(max_length=255)
    NAICS = models.CharField(max_length=255)
    HasBeenProfitable = models.BooleanField()
    HasBankruptedInLast7Years = models.BooleanField()
    InceptionDate = models.CharField(max_length=255)
    Address = models.OneToOneField('Address', on_delete=models.CASCADE)
    SelfReportedCashFlow = models.OneToOneField(
        'SelfReportedCashFlow', on_delete=models.CASCADE)


class Loan(models.Model):
    """ Loan model """

    RequestHeader = models.OneToOneField(
        'RequestHeader', on_delete=models.CASCADE)
    Business = models.OneToOneField('Business', on_delete=models.CASCADE)
    CFApplicationData = models.OneToOneField(
        'CFApplicationData', on_delete=models.CASCADE)


class Owner(models.Model):
    """ Owner model inside Loan """

    Name = models.CharField(max_length=255)
    FirstName = models.CharField(max_length=255)
    LastName = models.CharField(max_length=255)
    Email = models.CharField(max_length=255)
    DateOfBirth = models.CharField(max_length=255)
    HomePhone = models.CharField(max_length=255)
    SSN = models.CharField(max_length=255)
    PercentageOfOwnership = models.IntegerField()
    Loan = models.ForeignKey(
        'Loan', related_name='Owners', on_delete=models.CASCADE)
    HomeAddress = models.OneToOneField('Address', on_delete=models.CASCADE)


class CFApplicationData(models.Model):
    """ CFApplicationData Model inside Loan """

    RequestedLoanAmount = models.CharField(max_length=255)
    StatedCreditHistory = models.IntegerField()
    LegalEntityType = models.CharField(max_length=255)
    FilterID = models.CharField(max_length=255)


class SelfReportedCashFlow(models.Model):
    """ SelfReportedCashFlow model inside Business"""

    AnnualRevenue = models.FloatField()
    MonthlyAverageBankBalance = models.FloatField()
    MonthlyAverageCreditCardVolume = models.FloatField()


class LoanStatus(models.Model):
    """ Loan Status model """

    # LoanID = models.IntegerField()
    StatusMessage = models.CharField(max_length=255)
    Status = models.BooleanField()
    LoanID = models.IntegerField()
